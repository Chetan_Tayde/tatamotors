import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'IoT Dashboard',
    icon: 'nb-home',
    link: '/pages/iot-dashboard',
  },
  {
    title: 'Machine List',
    icon: 'nb-star',
    children: [
      {
        title: 'Machine 1',
        link: '/pages/extra-components/calendar',
      },
      {
        title: 'Machine 2',
        link: '/pages/extra-components/stepper',
      },
      {
        title: 'Machine 3',
        link: '/pages/extra-components/list',
      },
      {
        title: 'Machine 4',
        link: '/pages/extra-components/infinite-list',
      },
      {
        title: 'Machine 5',
        link: '/pages/extra-components/accordion',
      }
    ]
  }
];
