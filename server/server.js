const express  = require('express')
const log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'server.js';
const userRoutes = require('./routes/userRoutes');

let app  =  express();
let port =5000;


//routes initializes
app.all('/api/v1', userRoutes);



app.get('/', (req, res)=>{
    logger.info('index serving');
    res.send('index');
});


app.listen(port, ()=>{
    logger.info('server successfully started');
})

